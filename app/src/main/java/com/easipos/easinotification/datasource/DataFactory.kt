package com.easipos.easinotification.datasource

import android.app.Application
import com.easipos.easinotification.api.services.Api
import com.easipos.easinotification.datasource.auth.AuthDataSource
import com.easipos.easinotification.datasource.auth.AuthDataStore
import com.easipos.easinotification.datasource.notification.NotificationDataSource
import com.easipos.easinotification.datasource.notification.NotificationDataStore
import com.easipos.easinotification.datasource.precheck.PrecheckDataSource
import com.easipos.easinotification.datasource.precheck.PrecheckDataStore
import io.github.anderscheow.library.executor.PostExecutionThread
import io.github.anderscheow.library.executor.ThreadExecutor

class DataFactory(private val application: Application,
                  private val api: Api,
                  private val threadExecutor: ThreadExecutor,
                  private val postExecutionThread: PostExecutionThread
) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api)

    fun createAuthDataSource(): AuthDataStore =
        AuthDataSource(api)

    fun createNotificationDataSource(): NotificationDataStore =
        NotificationDataSource(api)
}
