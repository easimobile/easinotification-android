package com.easipos.easinotification.datasource.auth

import com.easipos.easinotification.api.requests.auth.LoginRequestModel
import com.easipos.easinotification.mapper.auth.LoginInfoMapper
import com.easipos.easinotification.models.LoginInfo
import com.easipos.easinotification.models.Result

interface AuthDataStore {

    suspend fun login(model: LoginRequestModel, loginInfoMapper: LoginInfoMapper): Result<LoginInfo>
}
