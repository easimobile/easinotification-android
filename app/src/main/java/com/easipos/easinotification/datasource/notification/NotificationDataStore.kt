package com.easipos.easinotification.datasource.notification

import com.easipos.easinotification.api.requests.BasicRequestModel
import com.easipos.easinotification.api.requests.notification.*
import com.easipos.easinotification.mapper.notification.NotificationMapper
import com.easipos.easinotification.mapper.notification.SenderMapper
import com.easipos.easinotification.models.DataSourceListModel
import com.easipos.easinotification.models.Notification
import com.easipos.easinotification.models.Result
import com.easipos.easinotification.models.Sender

interface NotificationDataStore {

    suspend fun registerFcmToken(model: RegisterFcmTokenRequestModel): Result<Nothing>

    suspend fun removeFcmToken(model: RemoveFcmTokenRequestModel): Result<Nothing>

    suspend fun getSenders(model: SenderPagingRequestModel, senderMapper: SenderMapper): Result<DataSourceListModel<Sender>>

    suspend fun getNotifications(model: NotificationPagingRequestModel, notificationMapper: NotificationMapper): Result<DataSourceListModel<Notification>>

    suspend fun archiveNotification(model: ArchiveNotificationRequestModel): Result<Nothing>

    suspend fun deleteNotification(model: DeleteNotificationRequestModel): Result<Nothing>

    suspend fun readNotification(model: ReadNotificationRequestModel): Result<Nothing>

    suspend fun getNotificationCount(model: BasicRequestModel): Result<Int>
}
