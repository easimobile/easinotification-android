package com.easipos.easinotification.datasource.notification

import com.easipos.easinotification.api.misc.parseException
import com.easipos.easinotification.api.misc.parseResponse
import com.easipos.easinotification.api.requests.BasicRequestModel
import com.easipos.easinotification.api.requests.notification.*
import com.easipos.easinotification.api.services.Api
import com.easipos.easinotification.mapper.notification.NotificationMapper
import com.easipos.easinotification.mapper.notification.SenderMapper
import com.easipos.easinotification.models.DataSourceListModel
import com.easipos.easinotification.models.Notification
import com.easipos.easinotification.models.Result
import com.easipos.easinotification.models.Sender

class NotificationDataSource(private val api: Api) : NotificationDataStore {

    override suspend fun registerFcmToken(model: RegisterFcmTokenRequestModel): Result<Nothing> {
        return try {
            val response = api.registerFcmToken(model)
            parseResponse(response)
        } catch (ex: Exception) {
            parseException(ex)
        }
    }

    override suspend fun removeFcmToken(model: RemoveFcmTokenRequestModel): Result<Nothing> {
        return try {
            val response = api.removeFcmToken(model)
            parseResponse(response)
        } catch (ex: Exception) {
            parseException(ex)
        }
    }

    override suspend fun getSenders(model: SenderPagingRequestModel, senderMapper: SenderMapper): Result<DataSourceListModel<Sender>> {
        return try {
            val response = api.getSenders(model)
            parseResponse(response) {
                DataSourceListModel(
                    itemList = senderMapper.transform(it.data),
                    totalOfElements = response.data!!.total,
                    hasNext = response.data.lastPage > response.data.currentPage,
                    currentPage = response.data.currentPage
                )
            }
        } catch (ex: Exception) {
            parseException(ex)
        }
    }

    override suspend fun getNotifications(model: NotificationPagingRequestModel, notificationMapper: NotificationMapper): Result<DataSourceListModel<Notification>> {
        return try {
            val response = api.getNotifications(model)
            parseResponse(response) {
                DataSourceListModel(
                    itemList = notificationMapper.transform(it.data),
                    totalOfElements = response.data!!.total,
                    hasNext = response.data.lastPage > response.data.currentPage,
                    currentPage = response.data.currentPage
                )
            }
        } catch (ex: Exception) {
            parseException(ex)
        }
    }

    override suspend fun archiveNotification(model: ArchiveNotificationRequestModel): Result<Nothing> {
        return try {
            val response = api.archiveNotifications(model)
            parseResponse(response)
        } catch (ex: Exception) {
            parseException(ex)
        }
    }

    override suspend fun deleteNotification(model: DeleteNotificationRequestModel): Result<Nothing> {
        return try {
            val response = api.deleteNotifications(model)
            parseResponse(response)
        } catch (ex: Exception) {
            parseException(ex)
        }
    }

    override suspend fun readNotification(model: ReadNotificationRequestModel): Result<Nothing> {
        return try {
            val response = api.readNotifications(model)
            parseResponse(response)
        } catch (ex: Exception) {
            parseException(ex)
        }
    }

    override suspend fun getNotificationCount(model: BasicRequestModel): Result<Int> {
        return try {
            val response = api.getNotificationCount(model)
            parseResponse(response) {
                it
            }
        } catch (ex: Exception) {
            parseException(ex)
        }
    }
}
