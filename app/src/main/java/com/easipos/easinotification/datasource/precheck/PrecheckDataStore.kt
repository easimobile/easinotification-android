package com.easipos.easinotification.datasource.precheck

import com.easipos.easinotification.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easinotification.models.Result

interface PrecheckDataStore {

    suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean>
}
