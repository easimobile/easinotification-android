package com.easipos.easinotification.datasource.auth

import com.easipos.easinotification.api.misc.parseException
import com.easipos.easinotification.api.misc.parseResponse
import com.easipos.easinotification.api.requests.auth.LoginRequestModel
import com.easipos.easinotification.api.services.Api
import com.easipos.easinotification.mapper.auth.LoginInfoMapper
import com.easipos.easinotification.models.LoginInfo
import com.easipos.easinotification.models.Result

class AuthDataSource(private val api: Api) : AuthDataStore {

    override suspend fun login(model: LoginRequestModel, loginInfoMapper: LoginInfoMapper): Result<LoginInfo> {
        return try {
            val response = api.login(model)
            parseResponse(response) {
                loginInfoMapper.transform(it)
            }
        } catch (ex: Exception) {
            parseException(ex)
        }
    }
}
