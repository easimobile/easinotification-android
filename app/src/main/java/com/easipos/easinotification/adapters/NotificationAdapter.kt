package com.easipos.easinotification.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.easinotification.R
import com.easipos.easinotification.databinding.ViewLoadingBinding
import com.easipos.easinotification.databinding.ViewNotificationBinding
import com.easipos.easinotification.models.Notification
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer

class NotificationAdapter(
    context: Context,
    private val listener: OnGestureDetectedListener
)
    : BaseRecyclerViewAdapter<Notification>(context) {

    companion object {
        const val VIEW_TYPE_LOADING = 0
        const val VIEW_TYPE_NORMAL = 1
    }

    interface OnGestureDetectedListener {
        fun onSelectItem(item: Notification)
    }

    private var isLoaderVisible: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_LOADING) {
            val binding = DataBindingUtil.inflate<ViewLoadingBinding>(
                layoutInflater, R.layout.view_loading, parent, false
            )
            LoadingViewHolder(binding)
        } else {
            val binding = DataBindingUtil.inflate<ViewNotificationBinding>(
                layoutInflater, R.layout.view_notification, parent, false
            )
            NotificationViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is NotificationViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isLoaderVisible) {
            if (position == items.size - 1) VIEW_TYPE_LOADING else VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    fun addCustomItems(items: List<Notification>) {
        this.items = (this.items + items).toMutableList()
    }

    fun addLoading() {
        isLoaderVisible = true
        items.add(Notification.EMPTY)
        notifyItemInserted(items.size - 1)
    }

    fun removeLoading() {
        isLoaderVisible = false
        val position = items.size - 1
        val item = items.getOrNull(position)
        if (item != null) {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    inner class NotificationViewHolder(private val binding: ViewNotificationBinding)
        : BaseViewHolder<Notification>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: Notification) {
        }

        override fun onClick(view: View, item: Notification?) {
            item?.let {
                listener.onSelectItem(item)
            }
        }
    }

    inner class LoadingViewHolder(private val binding: ViewLoadingBinding)
        : BaseViewHolder<String>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: String) {
        }

        override fun onClick(view: View, item: String?) {
        }
    }
}