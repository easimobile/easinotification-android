package com.easipos.easinotification.adapters

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.easipos.easinotification.R
import com.easipos.easinotification.databinding.ViewLoadingBinding
import com.easipos.easinotification.databinding.ViewSenderBinding
import com.easipos.easinotification.models.Sender
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_sender.*

class SenderAdapter(
    context: Context,
    private val listener: OnGestureDetectedListener
)
    : BaseRecyclerViewAdapter<Sender>(context) {

    companion object {
        const val VIEW_TYPE_LOADING = 0
        const val VIEW_TYPE_NORMAL = 1
    }

    interface OnGestureDetectedListener {
        fun onSelectItem(item: Sender)

        fun onArchive(itemsLeft: Int, item: Sender)

        fun onDelete(itemsLeft: Int, item: Sender)
    }

    private var viewBinderHelper: ViewBinderHelper? = null
    private var isLoaderVisible: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_LOADING) {
            val binding = DataBindingUtil.inflate<ViewLoadingBinding>(
                layoutInflater, R.layout.view_loading, parent, false
            )
            LoadingViewHolder(binding)
        } else {
            val binding = DataBindingUtil.inflate<ViewSenderBinding>(
                layoutInflater, R.layout.view_sender, parent, false
            )
            SenderViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SenderViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isLoaderVisible) {
            if (position == items.size - 1) VIEW_TYPE_LOADING else VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    fun setCustomItems(items: List<Sender>) {
        viewBinderHelper = ViewBinderHelper().apply {
            this.setOpenOnlyOne(true)
        }
        this.items = items.toMutableList()
    }

    fun addCustomItems(items: List<Sender>) {
        this.items = (this.items + items).toMutableList()
    }

    fun saveStates(outState: Bundle?) {
        viewBinderHelper?.saveStates(outState)
    }

    fun restoreStates(inState: Bundle?) {
        viewBinderHelper?.restoreStates(inState)
    }

    fun addLoading() {
        isLoaderVisible = true
        items.add(Sender.EMPTY)
        notifyItemInserted(items.size - 1)
    }

    fun removeLoading() {
        isLoaderVisible = false
        val position = items.size - 1
        val item = items.getOrNull(position)
        if (item != null) {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun readNotification(sender: String) {
        items.find { it.sender == sender }?.let { item ->
            val index = items.indexOf(item)
            items[index] = item.copy(isRead = true)
            notifyItemChanged(index)
        }
    }

    private fun selectNotification(sender: Sender) {
        listener.onSelectItem(sender)
    }

    private fun archiveNotification(sender: Sender) {
        listener.onArchive(items.size - 1, sender)

        val index = items.indexOf(sender)
        items.removeAt(index)
        notifyItemRemoved(index)
    }

    private fun deleteNotification(sender: Sender) {
        listener.onDelete(items.size - 1, sender)

        val index = items.indexOf(sender)
        items.removeAt(index)
        notifyItemRemoved(index)
    }

    inner class SenderViewHolder(private val binding: ViewSenderBinding)
        : BaseViewHolder<Sender>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: Sender) {
            viewBinderHelper?.bind(swipe_reveal_layout, item.sender)

            layout_content.post {
                layout_swipe_right.post {
                    layout_swipe_right.layoutParams = layout_swipe_right.layoutParams.apply {
                        this.height = layout_content.height
                    }
                }
            }

            layout_content.click {
                selectNotification(item)
            }

            text_view_archive.click {
                archiveNotification(item)
            }

            text_view_delete.click {
                deleteNotification(item)
            }
        }

        override fun onClick(view: View, item: Sender?) {
        }
    }

    inner class LoadingViewHolder(private val binding: ViewLoadingBinding)
        : BaseViewHolder<String>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: String) {
        }

        override fun onClick(view: View, item: String?) {
        }
    }
}