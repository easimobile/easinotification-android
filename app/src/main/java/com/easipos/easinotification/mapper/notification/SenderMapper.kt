package com.easipos.easinotification.mapper.notification

import com.easipos.easinotification.api.responses.notification.SenderResponseModel
import com.easipos.easinotification.mapper.Mapper
import com.easipos.easinotification.models.Sender

class SenderMapper : Mapper<Sender, SenderResponseModel>() {

    override fun transform(item: SenderResponseModel): Sender {
        return Sender(
            sender = item.sender ?: "",
            latestNotificationBody = item.latestNotificationBody ?: "",
            latestCreatedAt = item.latestCreatedAt ?: "",
            isRead = item.isRead == "1",
            isArchived = item.isArchived == "1"
        )
    }
}