package com.easipos.easinotification.mapper.auth

import com.easipos.easinotification.api.responses.auth.LoginInfoResponseModel
import com.easipos.easinotification.mapper.Mapper
import com.easipos.easinotification.models.LoginInfo

class LoginInfoMapper : Mapper<LoginInfo, LoginInfoResponseModel>() {

    override fun transform(item: LoginInfoResponseModel): LoginInfo {
        return LoginInfo(
            apiKey = item.apiKey ?: ""
        )
    }
}