package com.easipos.easinotification.mapper.notification

import com.easipos.easinotification.api.responses.notification.NotificationResponseModel
import com.easipos.easinotification.mapper.Mapper
import com.easipos.easinotification.models.Notification

class NotificationMapper : Mapper<Notification, NotificationResponseModel>() {

    override fun transform(item: NotificationResponseModel): Notification {
        return Notification(
            notificationId = item.notificationId ?: "",
            sender = item.sender ?: "",
            createdAt = item.createdAt ?: "",
            title = item.title ?: "",
            body = item.body ?: "",
            category = item.category ?: "",
            attachment = item.attachment ?: ""
        )
    }
}