package com.easipos.easinotification.event_bus

data class NotificationCount(val count: Int)