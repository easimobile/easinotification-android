package com.easipos.easinotification.constant

enum class NotificationCategory(val value: String) {

    INFO("INFO"),
    WARNING("WARNING"),
    ERROR("ERROR");

    companion object {
        fun parse(value: String): NotificationCategory? {
            return when (value) {
                "INFO" -> INFO
                "WARNING" -> WARNING
                "ERROR" -> ERROR
                else -> null
            }
        }
    }
}