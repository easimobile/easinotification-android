package com.easipos.easinotification.tools;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationListener extends RecyclerView.OnScrollListener {

  public int pageToLoad = 1;

  @NonNull
  private final LinearLayoutManager layoutManager;

  /**
   * Set scrolling threshold here (for now i'm assuming 10 item in one page)
   */
  private static final int PAGE_SIZE = 10;

  /**
   * Supporting only LinearLayoutManager for now.
   */
  public PaginationListener(@NonNull LinearLayoutManager layoutManager) {
    this.layoutManager = layoutManager;
  }

  @Override
  public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
    super.onScrolled(recyclerView, dx, dy);

    int visibleItemCount = layoutManager.getChildCount();
    int totalItemCount = layoutManager.getItemCount();
    int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

    if (!isLoading() && hasNext()) {
      if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
              && firstVisibleItemPosition >= 0
              && totalItemCount >= PAGE_SIZE) {
        loadMoreItems(pageToLoad);
      }
    }
  }

  protected abstract void loadMoreItems(int nextPage);

  public abstract boolean hasNext();

  public abstract boolean isLoading();
}