package com.easipos.easinotification.tools

import android.graphics.Color
import android.graphics.drawable.Drawable
import androidx.databinding.BindingAdapter
import com.easipos.easinotification.R
import com.easipos.easinotification.constant.NotificationCategory
import com.google.android.material.textview.MaterialTextView
import io.github.anderscheow.library.kotlinExt.findDrawable

object BindingAdapterUtil {

    @BindingAdapter("setCategory")
    @JvmStatic
    fun setCategoryIcon(materialTextView: MaterialTextView, category: String?) {
        category?.let {
            var drawable: Drawable? = null
            var textColor: Int = Color.parseColor("#56608E")
            var text: String? = null

            NotificationCategory.parse(it)?.let { notificationCategory ->
                when (notificationCategory) {
                    NotificationCategory.INFO -> {
                        drawable = materialTextView.context.findDrawable(R.drawable.ic_info)
                        textColor = Color.parseColor("#56608E")
                        text = materialTextView.context.getString(R.string.label_info)
                    }

                    NotificationCategory.WARNING -> {
                        drawable = materialTextView.context.findDrawable(R.drawable.ic_warning)
                        textColor = Color.parseColor("#efc15b")
                        text = materialTextView.context.getString(R.string.label_warning)
                    }

                    NotificationCategory.ERROR -> {
                        drawable = materialTextView.context.findDrawable(R.drawable.ic_error)
                        textColor = Color.parseColor("#ff4545")
                        text = materialTextView.context.getString(R.string.label_error)
                    }
                }
            }

            materialTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, drawable, null)
            materialTextView.setTextColor(textColor)
            materialTextView.text = text ?: category
        }
    }
}