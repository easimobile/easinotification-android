package com.easipos.easinotification.activities.login.navigation

import android.app.Activity
import com.easipos.easinotification.activities.main.MainActivity

class LoginNavigationImpl : LoginNavigation {

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}