package com.easipos.easinotification.activities.main.mvp

import com.easipos.easinotification.base.View

interface MainView : View {

    fun getNotificationCount()

    fun populateNotificationCount(count: Int)

    fun refreshActiveNotifications()

    fun refreshArchiveNotifications()

    fun navigateToLogin()
}
