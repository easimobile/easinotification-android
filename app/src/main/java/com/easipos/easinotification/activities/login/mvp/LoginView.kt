package com.easipos.easinotification.activities.login.mvp

import com.easipos.easinotification.base.View

interface LoginView : View {

    fun navigateToMain()
}
