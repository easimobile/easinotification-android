package com.easipos.easinotification.activities.login.mvp

import android.app.Application
import com.easipos.easinotification.api.requests.auth.LoginRequestModel
import com.easipos.easinotification.base.Presenter
import com.easipos.easinotification.managers.UserManager
import com.easipos.easinotification.models.Auth
import com.easipos.easinotification.models.LoginInfo
import com.easipos.easinotification.models.Result
import com.easipos.easinotification.repositories.auth.AuthRepository
import com.easipos.easinotification.tools.Preference
import com.easipos.easinotification.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class LoginPresenter(application: Application)
    : Presenter<LoginView>(application) {

    private val authRepository by instance<AuthRepository>()

    fun doLogin(model: LoginRequestModel, clientName: String) {
        view?.setLoadingIndicator(true)
        launch {
            val result = authRepository.login(model)
            withContext(Dispatchers.Main) {
                view?.setLoadingIndicator(false)
                when (result) {
                    is Result.Success<LoginInfo> -> {
                        UserManager.token = Auth(result.data.apiKey)
                        Preference.prefClientName = clientName

                        view?.navigateToMain()
                    }

                    is Result.Error -> {
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {}
                }
            }
        }
    }
}