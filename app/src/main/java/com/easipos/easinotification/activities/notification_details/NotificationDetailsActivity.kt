package com.easipos.easinotification.activities.notification_details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easinotification.R
import com.easipos.easinotification.base.CustomBaseAppCompatActivity
import com.easipos.easinotification.bundle.ParcelData
import com.easipos.easinotification.models.Notification
import com.easipos.easinotification.tools.BindingAdapterUtil
import com.easipos.easinotification.util.loadImage
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.visible
import kotlinx.android.synthetic.main.activity_notification_details.*
import org.jetbrains.anko.longToast

class NotificationDetailsActivity : CustomBaseAppCompatActivity() {

    companion object {
        fun newIntent(context: Context, notification: Notification): Intent {
            return Intent(context, NotificationDetailsActivity::class.java).apply {
                this.putExtra(ParcelData.NOTIFICATION, notification)
            }
        }
    }

    private val notification by argument<Notification>(ParcelData.NOTIFICATION)

    override fun onPause() {
        video_view.pausePlayer()
        super.onPause()
    }

    override fun onDestroy() {
        video_view.stopPlayer()
        super.onDestroy()
    }

    override fun getResLayout(): Int = R.layout.activity_notification_details

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        image_view.gone()
        video_view.gone()

        notification?.let { notification ->
            toolbar.title = notification.sender
            text_view_title.text = notification.title
            text_view_body.text = notification.body
            text_view_date.text = notification.formatCreatedAt(this)

            notification.attachment.takeIf { it.isNotBlank() }?.let { attachment ->
                Logger.d(attachment)
                val split = attachment.split(".")
                if (split.isNotEmpty()) {
                    val imageExtensions = listOf("jpg", "jpeg", "gif", "png")
                    val extension = split.last()

                    try {
                        if (imageExtensions.contains(extension)) {
                            image_view.visible()
                            image_view.loadImage(attachment)
                        } else {
                            video_view.visible()
                            video_view.setSource(attachment)
                        }
                    } catch (ex: Exception) {
                        longToast(ex.message ?: "")
                    }
                }
            }

            BindingAdapterUtil.setCategoryIcon(text_view_category, notification.category)
        }
    }

    private fun setupListeners() {
        toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}