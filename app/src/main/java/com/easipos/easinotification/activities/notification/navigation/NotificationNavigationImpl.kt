package com.easipos.easinotification.activities.notification.navigation

import android.app.Activity
import com.easipos.easinotification.activities.notification_details.NotificationDetailsActivity
import com.easipos.easinotification.models.Notification

class NotificationNavigationImpl : NotificationNavigation {

    override fun navigateToNotificationDetails(activity: Activity, notification: Notification) {
        activity.startActivity(NotificationDetailsActivity.newIntent(activity, notification))
    }
}