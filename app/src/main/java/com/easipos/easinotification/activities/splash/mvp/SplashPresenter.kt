package com.easipos.easinotification.activities.splash.mvp

import android.app.Application
import com.easipos.easinotification.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easinotification.base.Presenter
import com.easipos.easinotification.managers.UserManager
import com.easipos.easinotification.models.Result
import com.easipos.easinotification.repositories.precheck.PrecheckRepository
import com.easipos.easinotification.tools.Preference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class SplashPresenter(application: Application)
    : Presenter<SplashView>(application) {

    private val precheckRepository by instance<PrecheckRepository>()

    fun checkVersion() {
        launch {
            val result = precheckRepository.checkVersion(CheckVersionRequestModel())
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<Boolean> -> {
                        if (result.data) {
                            view?.showUpdateAppDialog()
                        } else {
                            checkIsAuthenticated()
                        }
                    }

                    is Result.Error -> {
                        checkIsAuthenticated()
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun checkIsAuthenticated() {
        if (Preference.prefIsLoggedIn && UserManager.token != null) {
            view?.navigateToMain()
        } else {
            view?.navigateToLogin()
        }
    }
}

