package com.easipos.easinotification.activities.splash.mvp

import com.easipos.easinotification.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
