package com.easipos.easinotification.activities.main.navigation

import android.app.Activity

interface MainNavigation {

    fun navigateToLogin(activity: Activity)

    fun navigateToNotification(activity: Activity, sender: String, isArchived: Boolean)
}