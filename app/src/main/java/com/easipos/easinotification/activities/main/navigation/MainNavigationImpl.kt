package com.easipos.easinotification.activities.main.navigation

import android.app.Activity
import com.easipos.easinotification.activities.login.LoginActivity
import com.easipos.easinotification.activities.notification.NotificationActivity

class MainNavigationImpl : MainNavigation {

    override fun navigateToLogin(activity: Activity) {
        activity.startActivity(LoginActivity.newIntent(activity))
        activity.finishAffinity()
    }

    override fun navigateToNotification(activity: Activity, sender: String, isArchived: Boolean) {
        activity.startActivity(NotificationActivity.newIntent(activity, sender, isArchived))
    }
}