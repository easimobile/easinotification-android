package com.easipos.easinotification.activities.notification.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.easipos.easinotification.api.requests.notification.NotificationPagingRequestModel
import com.easipos.easinotification.base.CustomBaseAndroidViewModel
import com.easipos.easinotification.models.DataSourceListModel
import com.easipos.easinotification.models.Notification
import com.easipos.easinotification.models.Result
import com.easipos.easinotification.repositories.notification.NotificationRepository
import com.easipos.easinotification.util.ErrorUtil
import io.github.anderscheow.library.kotlinExt.then
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

class NotificationViewModel(application: Application) : CustomBaseAndroidViewModel<Void>(application) {

    //region Observable Field
    //endregion

    //region LiveData
    val notificationsLD = MutableLiveData<List<DataSourceListModel<Notification>>>()

    private var sender: String = ""
    private var isArchived: Boolean = false
    //endregion

    private val notificationRepository by instance<NotificationRepository>()

    override fun start(args: Void?) {
    }

    override fun onRefresh() {
        getNotifications(sender, isArchived)
    }

    fun getNotifications(sender: String, isArchived: Boolean, page: Int = 1) {
        this.sender = sender
        this.isArchived = isArchived

        setIsLoading(true)
        viewModelScope.launch {
            val model = NotificationPagingRequestModel(
                page = page,
                sender = sender,
                isArchived = isArchived then "1" ?: "0"
            )
            when (val result = notificationRepository.getNotifications(model)) {
                is Result.Success<DataSourceListModel<Notification>> -> {
                    finishLoading(result.data.totalOfElements)
                    if (model.page == 1) {
                        notificationsLD.postValue(listOf(result.data))
                    } else {
                        notificationsLD.postValue((notificationsLD.value ?: listOf()) + result.data)
                    }
                }

                is Result.Error -> {
                    setIsLoading(false)
                    showToast(ErrorUtil.parseException(result.exception))
                }

                else -> {
                }
            }
        }
    }
}