package com.easipos.easinotification.activities.notification.navigation

import android.app.Activity
import com.easipos.easinotification.models.Notification

interface NotificationNavigation {

    fun navigateToNotificationDetails(activity: Activity, notification: Notification)
}