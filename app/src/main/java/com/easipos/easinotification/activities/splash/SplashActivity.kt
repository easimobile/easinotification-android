package com.easipos.easinotification.activities.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easinotification.R
import com.easipos.easinotification.activities.splash.mvp.SplashPresenter
import com.easipos.easinotification.activities.splash.mvp.SplashView
import com.easipos.easinotification.activities.splash.navigation.SplashNavigation
import com.easipos.easinotification.base.CustomBaseAppCompatActivity
import io.github.anderscheow.library.kotlinExt.rate
import org.kodein.di.generic.instance

class SplashActivity : CustomBaseAppCompatActivity(), SplashView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SplashActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<SplashNavigation>()

    private val presenter by lazy { SplashPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_splash

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        presenter.checkIsAuthenticated()
    }
    //endregion

    //region SplashView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun showUpdateAppDialog() {
        showYesAlertDialog(getString(R.string.prompt_update_app), buttonText = R.string.action_upgrade_now) {
            this@SplashActivity.rate()
            finishAffinity()
        }
    }

    override fun navigateToLogin() {
        navigation.navigateToLogin(this)
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }
    //endregion

    //region Action Methods
    private fun checkVersion() {
        presenter.checkVersion()
    }
    //endregion
}
