package com.easipos.easinotification.activities.main.mvp

import android.app.Application
import com.easipos.easinotification.Easi
import com.easipos.easinotification.api.requests.BasicRequestModel
import com.easipos.easinotification.api.requests.notification.ArchiveNotificationRequestModel
import com.easipos.easinotification.api.requests.notification.DeleteNotificationRequestModel
import com.easipos.easinotification.api.requests.notification.ReadNotificationRequestModel
import com.easipos.easinotification.base.Presenter
import com.easipos.easinotification.managers.FcmManager
import com.easipos.easinotification.models.Result
import com.easipos.easinotification.repositories.notification.NotificationRepository
import io.github.anderscheow.library.kotlinExt.delay
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class MainPresenter(application: Application)
    : Presenter<MainView>(application) {

    private val notificationRepository by instance<NotificationRepository>()
    private val fcmManager by kodein.instance<FcmManager>()

    fun doLogout() {
        fcmManager.service.removeFcmToken()

        delay(1000) {
            (application as? Easi)?.logout()
            view?.navigateToLogin()
        }
    }

    fun doGetNotificationCount() {
        launch {
            val result = notificationRepository.getNotificationCount(BasicRequestModel())
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<Int> -> {
                        view?.populateNotificationCount(result.data)
                    }

                    is Result.Error -> {
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doArchiveNotification(model: ArchiveNotificationRequestModel) {
        if (model.archiveAll != null && model.archiveAll!!) {
            view?.setLoadingIndicator(true)
        }
        launch {
            val result = notificationRepository.archiveNotification(model)
            withContext(Dispatchers.Main) {
                view?.setLoadingIndicator(false)
                when (result) {
                    is Result.EmptySuccess -> {
                        if (model.archiveAll != null && model.archiveAll!!) {
                            view?.refreshActiveNotifications()
                        } else {
                            view?.refreshArchiveNotifications()
                        }

                        view?.getNotificationCount()
                    }

                    is Result.Error -> {
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doDeleteNotification(model: DeleteNotificationRequestModel) {
        if (model.deleteAll != null && model.deleteAll!!) {
            view?.setLoadingIndicator(true)
        }
        launch {
            val result = notificationRepository.deleteNotification(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.EmptySuccess -> {
                        if (model.deleteAll != null && model.deleteAll!!) {
                            view?.refreshArchiveNotifications()
                        }

                        view?.getNotificationCount()
                    }

                    is Result.Error -> {
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun doReadNotification(model: ReadNotificationRequestModel) {
        launch {
            val result = notificationRepository.readNotification(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.EmptySuccess -> {
                        view?.getNotificationCount()
                    }

                    is Result.Error -> {
                    }

                    else -> {
                    }
                }
            }
        }
    }
}
