package com.easipos.easinotification.activities.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easinotification.R
import com.easipos.easinotification.activities.login.mvp.LoginPresenter
import com.easipos.easinotification.activities.login.mvp.LoginView
import com.easipos.easinotification.activities.login.navigation.LoginNavigation
import com.easipos.easinotification.api.requests.auth.LoginRequestModel
import com.easipos.easinotification.base.CustomBaseAppCompatActivity
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.notBlank
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.generic.instance

class LoginActivity : CustomBaseAppCompatActivity(), LoginView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<LoginNavigation>()
    private val validator by instance<Validator>()

    private val presenter by lazy { LoginPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_login

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        showYesAlertDialog(message, buttonText = R.string.action_ok, action = action)
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }
    //endregion

    //region Action Methods
    private fun setupViews() {

    }

    private fun setupListeners() {
        button_sign_in.click {
            attemptSignIn()
        }
    }

    private fun attemptSignIn() {
        val clientNameValidation = text_input_layout_client_name.validate()
            .notBlank(R.string.error_field_required)

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val clientName = values[0].trim()

                val model = LoginRequestModel(
                    clientName = clientName
                )

                presenter.doLogin(model, clientName)
            }
        }).validate(clientNameValidation)
    }
    //endregion
}
