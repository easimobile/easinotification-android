package com.easipos.easinotification.activities.notification

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.easinotification.R
import com.easipos.easinotification.activities.notification.navigation.NotificationNavigation
import com.easipos.easinotification.activities.notification.viewmodel.NotificationViewModel
import com.easipos.easinotification.activities.notification.viewmodel.NotificationViewModelFactory
import com.easipos.easinotification.adapters.NotificationAdapter
import com.easipos.easinotification.base.CustomLifecycleActivity
import com.easipos.easinotification.bundle.ParcelData
import com.easipos.easinotification.models.Notification
import com.easipos.easinotification.tools.PaginationListener
import io.github.anderscheow.library.kotlinExt.argument
import kotlinx.android.synthetic.main.activity_notification.*
import org.kodein.di.generic.instance

class NotificationActivity : CustomLifecycleActivity<NotificationViewModel>(),
    NotificationAdapter.OnGestureDetectedListener {

    companion object {
        fun newIntent(context: Context, sender: String, isArchived: Boolean): Intent {
            return Intent(context, NotificationActivity::class.java).apply {
                this.putExtra(ParcelData.SENDER, sender)
                this.putExtra(ParcelData.IS_ARCHIVED, isArchived)
            }
        }
    }

    private val navigation by instance<NotificationNavigation>()
    private val factory by lazy { NotificationViewModelFactory(application) }

    private var notificationAdapter: NotificationAdapter? = null
    private var paginationListener: PaginationListener? = null

    private val sender by argument(ParcelData.SENDER, "")
    private val isArchived by argument(ParcelData.IS_ARCHIVED, false)

    override fun getResLayout(): Int = R.layout.activity_notification

    override fun setupViewModel(): NotificationViewModel {
        return ViewModelProvider(this, factory)
            .get(NotificationViewModel::class.java)
    }

    override fun setupViewModelObserver() {
        viewModel.notificationsLD.observe(this) { newList ->
            notificationAdapter?.removeLoading()

            if (newList.isNotEmpty()) {
                val last = newList.last()
                if (last.hasNext) {
                    paginationListener?.pageToLoad = last.currentPage + 1;
                }

                notificationAdapter?.items = newList.map { it.itemList }
                    .reduce { acc, list -> acc + list }.toMutableList()
            }
        }
    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        setupViews()
        setupListeners()

        viewModel.getNotifications(sender, isArchived)
    }

    override fun onSelectItem(item: Notification) {
        navigation.navigateToNotificationDetails(this, item)
    }

    private fun setupViews() {
        toolbar.title = sender

        recycler_view_notification.apply {
            notificationAdapter =
                NotificationAdapter(this@NotificationActivity, this@NotificationActivity)
            this.adapter = notificationAdapter
            this.layoutManager = LinearLayoutManager(this@NotificationActivity)
        }
    }

    private fun setupListeners() {
        toolbar.setNavigationOnClickListener {
            finish()
        }

        recycler_view_notification.apply {
            val layoutManager = this.layoutManager as LinearLayoutManager
            paginationListener = object : PaginationListener(layoutManager) {
                override fun loadMoreItems(nextPage: Int) {
                    viewModel.getNotifications(sender, isArchived, nextPage)
                    notificationAdapter?.addLoading()
                }

                override fun hasNext(): Boolean {
                    val list = viewModel.notificationsLD.value
                    return if (list != null && list.isNotEmpty()) {
                        list.last().hasNext
                    } else {
                        false
                    }
                }

                override fun isLoading(): Boolean {
                    return viewModel.isLoading.get()
                }
            }
            this.addOnScrollListener(paginationListener!!)
        }
    }
}