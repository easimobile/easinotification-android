package com.easipos.easinotification.activities.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.easipos.easinotification.R
import com.easipos.easinotification.activities.main.mvp.MainPresenter
import com.easipos.easinotification.activities.main.mvp.MainView
import com.easipos.easinotification.activities.main.navigation.MainNavigation
import com.easipos.easinotification.api.requests.notification.ArchiveNotificationRequestModel
import com.easipos.easinotification.api.requests.notification.DeleteNotificationRequestModel
import com.easipos.easinotification.api.requests.notification.ReadNotificationRequestModel
import com.easipos.easinotification.base.CustomBaseAppCompatActivity
import com.easipos.easinotification.fragments.sender.SenderFragment
import com.easipos.easinotification.managers.FcmManager
import com.easipos.easinotification.managers.PushNotificationManager
import com.google.android.material.tabs.TabLayoutMediator
import com.onesignal.OneSignal
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<MainNavigation>()
    private val fcmManager by instance<FcmManager>()
    private val pushNotificationManager by instance<PushNotificationManager>()

    private val presenter by lazy { MainPresenter(application) }

    private val activeSenderFragment by lazy { SenderFragment.newInstance(false) }
    private val archiveSenderFragment by lazy { SenderFragment.newInstance(true) }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        observeOneSignalSubscription()
        registerPushTokenIfPossible()
        processPayload()

        setupViews()
        setupListeners()

        getNotificationCount()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun populateNotificationCount(count: Int) {
        if (count > 0) {
            text_view_count.visible()

            if (count > 99) {
                text_view_count.text = "99"
            } else {
                text_view_count.text = count.toString()
            }
        } else {
            text_view_count.gone()
        }
    }

    override fun getNotificationCount() {
        presenter.doGetNotificationCount()
    }

    override fun refreshActiveNotifications() {
        activeSenderFragment.refreshSenders()
    }

    override fun refreshArchiveNotifications() {
        archiveSenderFragment.refreshSenders()
    }

    override fun navigateToLogin() {
        navigation.navigateToLogin(this)
    }
    //endregion

    //region Action Methods
    fun processPayload() {
        Logger.d(pushNotificationManager.payload)
        pushNotificationManager.payload?.arg?.let { arg ->
            readNotification(arg, false)
            pushNotificationManager.removePayload()
        }
    }

    fun refreshNotifications() {
        getNotificationCount()
        refreshActiveNotifications()
        refreshArchiveNotifications()
    }

    fun archiveNotification(model: ArchiveNotificationRequestModel) {
        presenter.doArchiveNotification(model)
    }

    fun deleteNotification(model: DeleteNotificationRequestModel) {
        presenter.doDeleteNotification(model)
    }

    fun readNotification(sender: String, isArchived: Boolean) {
        navigation.navigateToNotification(this, sender, isArchived)

        val model = ReadNotificationRequestModel(
            sender = sender
        )
        presenter.doReadNotification(model)

        if (isArchived) {
            archiveSenderFragment.readNotification(sender)
        } else {
            activeSenderFragment.readNotification(sender)
        }
    }

    private fun observeOneSignalSubscription() {
        OneSignal.addSubscriptionObserver { state ->
            Logger.d(state.toString())
            fcmManager.service.saveFcmToken(state.to.userId)
        }
    }

    private fun registerPushTokenIfPossible() {
        fcmManager.service.registerFcmToken()
    }

    private fun setupViews() {
        text_view_count.gone()

        viewPager.isUserInputEnabled = false
        viewPager.adapter = NotificationPagerAdapter(this)

        TabLayoutMediator(tab_layout, viewPager) { tab, position ->
            tab.text = (position == 0) then "Active" ?: "Archive"
            tab.icon = (position == 0) then findDrawable(R.drawable.ic_active) ?: findDrawable(R.drawable.ic_archive)
        }.attach()
    }

    private fun setupListeners() {
        image_view_logout.click {
            presenter.doLogout()
        }
    }
    //endregion

    private inner class NotificationPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = 2

        override fun createFragment(position: Int): Fragment = when (position) {
            0 -> activeSenderFragment
            else -> archiveSenderFragment
        }
    }
}
