package com.easipos.easinotification.activities.login.navigation

import android.app.Activity

interface LoginNavigation {

    fun navigateToMain(activity: Activity)
}