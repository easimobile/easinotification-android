package com.easipos.easinotification.util

import android.app.Activity
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.easipos.easinotification.Easi
import com.easipos.easinotification.managers.PushNotificationManager
import com.easipos.easinotification.managers.PushNotificationManager.Companion.FCM_BODY
import com.easipos.easinotification.managers.PushNotificationManager.Companion.FCM_TITLE
import com.easipos.easinotification.tools.Preference
import com.tapadoo.alerter.Alerter
import org.json.JSONObject

val languages = mapOf("zh" to "中文", "en" to "English")

fun defaultRequestOption(): RequestOptions {
    return RequestOptions()
}

fun ImageView.loadImage(imageUrl: String) {
    Glide.with(this.context)
        .load(imageUrl)
        .apply(defaultRequestOption())
        .transition(DrawableTransitionOptions().crossFade(500))
        .into(this)
}

fun changeLanguage(activity: Activity, language: String) {
    if (Preference.prefLanguageCode != language) {
        Preference.prefLanguageCode = language
        Easi.localeManager.setNewLocale(activity, language)
        activity.recreate()
    }
}

fun showAlerter(easi: Easi, pushNotificationManager: PushNotificationManager, jsonObject: JSONObject) {
    jsonObject.takeIf {
        jsonObject.has(FCM_TITLE) && jsonObject.has(FCM_BODY)
    }.run {
        this?.let { data ->
            easi.currentActivity?.let { activity ->
                Alerter.create(activity)
                    .setTitle(data.getString(FCM_TITLE) ?: "")
                    .setText(data.getString(FCM_BODY) ?: "")
                    .setBackgroundColorInt(Color.parseColor("#efc15b"))
                    .setDuration(5000)
                    .setIconColorFilter(Color.WHITE)
                    .enableSwipeToDismiss()
                    .enableVibration(true)
                    .setOnClickListener(View.OnClickListener {
                        Alerter.hide()

                        pushNotificationManager.openNotification(easi, jsonObject, true)
                    })
                    //.setOnShowListener { Tracker.trackScreen(this@CustomLifecycleActivity, Screen.IN_APP_NOTIFICATION) }
                    .show()
            }
        }
    }
}