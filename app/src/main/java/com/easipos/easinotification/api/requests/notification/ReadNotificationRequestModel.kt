package com.easipos.easinotification.api.requests.notification

import com.easipos.easinotification.managers.UserManager

data class ReadNotificationRequestModel(
    val sender: String,
    val apiKey: String? = UserManager.token?.token
)