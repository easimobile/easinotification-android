package com.easipos.easinotification.api

object ApiEndpoint {

    const val CHECK_VERSION = "ddcard/version/check"

    const val LOGIN = "login"

    const val REGISTER_PUSH_TOKEN = "api/registerPushToken"
    const val REMOVE_PUSH_TOKEN = "api/removePushToken"
    const val GET_SENDERS = "api/getSenders"
    const val GET_NOTIFICATIONS = "api/getNotifications"
    const val ARCHIVE_NOTIFICATION = "api/archiveNotification"
    const val DELETE_NOTIFICATION = "api/deleteNotification"
    const val READ_NOTIFICATION = "api/readNotification"
    const val GET_NOTIFICATION_COUNT = "api/getNotificationCount"
}
