package com.easipos.easinotification.api.requests.notification

import com.easipos.easinotification.managers.UserManager

class RegisterFcmTokenRequestModel(
    val pushToken: String,
    val os: String = "android",
    val apiKey: String? = UserManager.token?.token
)