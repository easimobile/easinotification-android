package com.easipos.easinotification.api.services

import com.easipos.easinotification.api.ApiEndpoint
import com.easipos.easinotification.api.misc.EmptyResponseModel
import com.easipos.easinotification.api.misc.ResponseModel
import com.easipos.easinotification.api.requests.BasicRequestModel
import com.easipos.easinotification.api.requests.auth.LoginRequestModel
import com.easipos.easinotification.api.requests.notification.*
import com.easipos.easinotification.api.responses.auth.LoginInfoResponseModel
import com.easipos.easinotification.api.responses.notification.NotificationPagingResponseModel
import com.easipos.easinotification.api.responses.notification.SenderPagingResponseModel
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {

    @POST(ApiEndpoint.CHECK_VERSION)
    suspend fun checkVersion(@Body body: RequestBody): ResponseModel<Boolean>

    @POST(ApiEndpoint.LOGIN)
    suspend fun login(@Body body: LoginRequestModel): ResponseModel<LoginInfoResponseModel>

    @POST(ApiEndpoint.REGISTER_PUSH_TOKEN)
    suspend fun registerFcmToken(@Body body: RegisterFcmTokenRequestModel): EmptyResponseModel

    @POST(ApiEndpoint.REMOVE_PUSH_TOKEN)
    suspend fun removeFcmToken(@Body body: RemoveFcmTokenRequestModel): EmptyResponseModel

    @POST(ApiEndpoint.GET_SENDERS)
    suspend fun getSenders(@Body body: SenderPagingRequestModel): ResponseModel<SenderPagingResponseModel>

    @POST(ApiEndpoint.GET_NOTIFICATIONS)
    suspend fun getNotifications(@Body body: NotificationPagingRequestModel): ResponseModel<NotificationPagingResponseModel>

    @POST(ApiEndpoint.ARCHIVE_NOTIFICATION)
    suspend fun archiveNotifications(@Body body: ArchiveNotificationRequestModel): EmptyResponseModel

    @POST(ApiEndpoint.DELETE_NOTIFICATION)
    suspend fun deleteNotifications(@Body body: DeleteNotificationRequestModel): EmptyResponseModel

    @POST(ApiEndpoint.READ_NOTIFICATION)
    suspend fun readNotifications(@Body body: ReadNotificationRequestModel): EmptyResponseModel

    @POST(ApiEndpoint.GET_NOTIFICATION_COUNT)
    suspend fun getNotificationCount(@Body body: BasicRequestModel): ResponseModel<Int>
}
