package com.easipos.easinotification.api.requests.notification

import com.easipos.easinotification.managers.UserManager

data class SenderPagingRequestModel(
    val apiKey: String? = UserManager.token?.token,
    val isArchived: String,
    val page: Int = 1,
)