package com.easipos.easinotification.api.requests.notification

import com.easipos.easinotification.managers.UserManager

data class DeleteNotificationRequestModel(
    val sender: String? = null,
    var deleteAll: Boolean? = null,
    val apiKey: String? = UserManager.token?.token
)