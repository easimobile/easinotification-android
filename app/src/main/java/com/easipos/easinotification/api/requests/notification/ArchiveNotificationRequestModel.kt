package com.easipos.easinotification.api.requests.notification

import com.easipos.easinotification.managers.UserManager

data class ArchiveNotificationRequestModel(
    val sender: String? = null,
    var archiveAll: Boolean? = null,
    val apiKey: String? = UserManager.token?.token
)