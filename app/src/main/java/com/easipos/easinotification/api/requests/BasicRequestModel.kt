package com.easipos.easinotification.api.requests

import com.easipos.easinotification.managers.UserManager

data class BasicRequestModel(
    val apiKey: String = UserManager.token?.token ?: ""
)