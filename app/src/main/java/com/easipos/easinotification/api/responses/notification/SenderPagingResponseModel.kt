package com.easipos.easinotification.api.responses.notification

import com.google.gson.annotations.SerializedName

data class SenderPagingResponseModel(
    @SerializedName("current_page")
    val currentPage: Int,

    @SerializedName("last_page")
    val lastPage: Int,

    @SerializedName("total")
    val total: Long,

    @SerializedName("data")
    val data: List<SenderResponseModel>
)

data class SenderResponseModel(
    @SerializedName("sender")
    val sender: String?,

    @SerializedName("latestNotificationBody")
    val latestNotificationBody: String?,

    @SerializedName("latestCreatedAt")
    val latestCreatedAt: String?,

    @SerializedName("isRead")
    val isRead: String?,

    @SerializedName("isArchived")
    val isArchived: String?
)