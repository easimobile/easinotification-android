package com.easipos.easinotification.api.requests.notification

import com.easipos.easinotification.managers.UserManager

data class NotificationPagingRequestModel(
    val apiKey: String? = UserManager.token?.token,
    val sender: String,
    val isArchived: String,
    val page: Int = 1,
)