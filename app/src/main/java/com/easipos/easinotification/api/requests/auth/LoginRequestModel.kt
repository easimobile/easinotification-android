package com.easipos.easinotification.api.requests.auth

data class LoginRequestModel(
    val clientName: String
)