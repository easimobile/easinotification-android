package com.easipos.easinotification.api.responses.notification

import com.google.gson.annotations.SerializedName

data class NotificationPagingResponseModel(
    @SerializedName("current_page")
    val currentPage: Int,

    @SerializedName("last_page")
    val lastPage: Int,

    @SerializedName("total")
    val total: Long,

    @SerializedName("data")
    val data: List<NotificationResponseModel>
)

data class NotificationResponseModel(
    @SerializedName("notificationId")
    val notificationId: String?,

    @SerializedName("sender")
    val sender: String?,

    @SerializedName("createdAt")
    val createdAt: String?,

    @SerializedName("title")
    val title: String?,

    @SerializedName("body")
    val body: String?,

    @SerializedName("category")
    val category: String?,

    @SerializedName("attachment")
    val attachment: String?
)