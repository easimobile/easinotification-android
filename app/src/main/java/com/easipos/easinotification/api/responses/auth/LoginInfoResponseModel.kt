package com.easipos.easinotification.api.responses.auth

import com.google.gson.annotations.SerializedName

data class LoginInfoResponseModel(
    @SerializedName("apiKey")
    val apiKey: String?
)