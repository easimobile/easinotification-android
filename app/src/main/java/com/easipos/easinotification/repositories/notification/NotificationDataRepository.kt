package com.easipos.easinotification.repositories.notification

import com.easipos.easinotification.api.requests.BasicRequestModel
import com.easipos.easinotification.api.requests.notification.*
import com.easipos.easinotification.datasource.DataFactory
import com.easipos.easinotification.mapper.notification.NotificationMapper
import com.easipos.easinotification.mapper.notification.SenderMapper
import com.easipos.easinotification.models.DataSourceListModel
import com.easipos.easinotification.models.Notification
import com.easipos.easinotification.models.Result
import com.easipos.easinotification.models.Sender

class NotificationDataRepository(private val dataFactory: DataFactory) : NotificationRepository {

    private val senderMapper by lazy { SenderMapper() }
    private val notificationMapper by lazy { NotificationMapper() }

    override suspend fun registerFcmToken(model: RegisterFcmTokenRequestModel): Result<Nothing> =
        dataFactory.createNotificationDataSource()
            .registerFcmToken(model)

    override suspend fun removeFcmToken(model: RemoveFcmTokenRequestModel): Result<Nothing> =
        dataFactory.createNotificationDataSource()
            .removeFcmToken(model)

    override suspend fun getSenders(model: SenderPagingRequestModel): Result<DataSourceListModel<Sender>> =
        dataFactory.createNotificationDataSource()
            .getSenders(model, senderMapper)

    override suspend fun getNotifications(model: NotificationPagingRequestModel): Result<DataSourceListModel<Notification>> =
        dataFactory.createNotificationDataSource()
            .getNotifications(model, notificationMapper)

    override suspend fun archiveNotification(model: ArchiveNotificationRequestModel): Result<Nothing> =
        dataFactory.createNotificationDataSource()
            .archiveNotification(model)

    override suspend fun deleteNotification(model: DeleteNotificationRequestModel): Result<Nothing> =
        dataFactory.createNotificationDataSource()
            .deleteNotification(model)

    override suspend fun readNotification(model: ReadNotificationRequestModel): Result<Nothing> =
        dataFactory.createNotificationDataSource()
            .readNotification(model)

    override suspend fun getNotificationCount(model: BasicRequestModel): Result<Int> =
        dataFactory.createNotificationDataSource()
            .getNotificationCount(model)
}