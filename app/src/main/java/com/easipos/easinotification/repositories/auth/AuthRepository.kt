package com.easipos.easinotification.repositories.auth

import com.easipos.easinotification.api.requests.auth.LoginRequestModel
import com.easipos.easinotification.models.LoginInfo
import com.easipos.easinotification.models.Result

interface AuthRepository {

    suspend fun login(model: LoginRequestModel): Result<LoginInfo>
}
