package com.easipos.easinotification.repositories.precheck

import com.easipos.easinotification.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easinotification.datasource.DataFactory
import com.easipos.easinotification.models.Result

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
