package com.easipos.easinotification.repositories.precheck

import com.easipos.easinotification.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easinotification.models.Result

interface PrecheckRepository {

    suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean>
}
