package com.easipos.easinotification.repositories.auth

import com.easipos.easinotification.api.requests.auth.LoginRequestModel
import com.easipos.easinotification.datasource.DataFactory
import com.easipos.easinotification.mapper.auth.LoginInfoMapper
import com.easipos.easinotification.models.LoginInfo
import com.easipos.easinotification.models.Result

class AuthDataRepository(private val dataFactory: DataFactory) : AuthRepository {

    private val loginInfoMapper by lazy { LoginInfoMapper() }

    override suspend fun login(model: LoginRequestModel): Result<LoginInfo> =
        dataFactory.createAuthDataSource()
            .login(model, loginInfoMapper)
}
