package com.easipos.easinotification.fragments.sender.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.easipos.easinotification.api.requests.notification.SenderPagingRequestModel
import com.easipos.easinotification.base.CustomBaseAndroidViewModel
import com.easipos.easinotification.models.DataSourceListModel
import com.easipos.easinotification.models.Result
import com.easipos.easinotification.models.Sender
import com.easipos.easinotification.repositories.notification.NotificationRepository
import com.easipos.easinotification.util.ErrorUtil
import io.github.anderscheow.library.kotlinExt.then
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

class SenderViewModel(application: Application) : CustomBaseAndroidViewModel<Void>(application) {

    //region Observable Field
    //endregion

    //region LiveData
    val sendersLD = MutableLiveData<List<DataSourceListModel<Sender>>>()

    private var isArchived: Boolean = false
    //endregion

    private val notificationRepository by instance<NotificationRepository>()

    override fun start(args: Void?) {
    }

    override fun onRefresh() {
        getSenders(isArchived)
    }

    fun getSenders(isArchived: Boolean, page: Int = 1) {
        this.isArchived = isArchived

        setIsLoading(true)
        viewModelScope.launch {
            val model = SenderPagingRequestModel(
                page = page,
                isArchived = isArchived then "1" ?: "0"
            )
            when (val result = notificationRepository.getSenders(model)) {
                is Result.Success<DataSourceListModel<Sender>> -> {
                    finishLoading(result.data.totalOfElements)
                    if (model.page == 1) {
                        sendersLD.postValue(listOf(result.data))
                    } else {
                        sendersLD.postValue((sendersLD.value ?: listOf()) + result.data)
                    }
                }

                is Result.Error -> {
                    setIsLoading(false)
                    showToast(ErrorUtil.parseException(result.exception))
                }

                else -> {
                }
            }
        }
    }
}