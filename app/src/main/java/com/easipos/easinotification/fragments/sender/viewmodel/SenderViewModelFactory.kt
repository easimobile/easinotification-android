package com.easipos.easinotification.fragments.sender.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SenderViewModelFactory(private val application: Application)
    : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SenderViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SenderViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}