package com.easipos.easinotification.fragments.sender

import android.os.Bundle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.easipos.easinotification.R
import com.easipos.easinotification.activities.main.MainActivity
import com.easipos.easinotification.adapters.SenderAdapter
import com.easipos.easinotification.api.requests.notification.ArchiveNotificationRequestModel
import com.easipos.easinotification.api.requests.notification.DeleteNotificationRequestModel
import com.easipos.easinotification.base.CustomLifecycleFragment
import com.easipos.easinotification.bundle.ParcelData
import com.easipos.easinotification.fragments.sender.viewmodel.SenderViewModel
import com.easipos.easinotification.fragments.sender.viewmodel.SenderViewModelFactory
import com.easipos.easinotification.models.Sender
import com.easipos.easinotification.tools.PaginationListener
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.library.kotlinExt.then
import io.github.anderscheow.library.kotlinExt.withActivityAs
import kotlinx.android.synthetic.main.fragment_sender.*

class SenderFragment : CustomLifecycleFragment<SenderViewModel>(),
    SenderAdapter.OnGestureDetectedListener {

    companion object {
        fun newInstance(isArchived: Boolean): SenderFragment {
            return SenderFragment().apply {
                this.arguments = Bundle().apply {
                    this.putBoolean(ParcelData.IS_ARCHIVED, isArchived)
                }
            }
        }
    }

    private val factory by lazy { SenderViewModelFactory(requireActivity().application) }

    private var senderAdapter: SenderAdapter? = null
    private var paginationListener: PaginationListener? = null

    private val isArchived by argument(ParcelData.IS_ARCHIVED, false)

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        senderAdapter?.restoreStates(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        senderAdapter?.saveStates(outState)
    }

    override fun getResLayout(): Int = R.layout.fragment_sender

    override fun setupViewModel(): SenderViewModel {
        return ViewModelProvider(this, factory)
            .get(SenderViewModel::class.java)
    }

    override fun setupViewModelObserver(lifecycleOwner: LifecycleOwner) {
        viewModel.sendersLD.observe(lifecycleOwner) { newList ->
            senderAdapter?.removeLoading()

            if (newList.isNotEmpty()) {
                val last = newList.last()
                if (last.hasNext) {
                    paginationListener?.pageToLoad = last.currentPage + 1;
                }

                senderAdapter?.setCustomItems(newList.map { it.itemList }
                    .reduce { acc, list -> acc + list })
            }
        }
    }

    override fun init() {
        super.init()
        setupViews()
        setupListeners()

        refreshSenders()
    }

    override fun onSelectItem(item: Sender) {
        withActivityAs<MainActivity>()?.readNotification(item.sender, item.isArchived)
    }

    override fun onArchive(itemsLeft: Int, item: Sender) {
        viewModel.listSize.apply {
            this.set(itemsLeft.toLong())
            this.notifyChange()
        }

        val model = ArchiveNotificationRequestModel(
            sender = item.sender
        )

        withActivityAs<MainActivity>()?.archiveNotification(model)
    }

    override fun onDelete(itemsLeft: Int, item: Sender) {
        viewModel.listSize.apply {
            this.set(itemsLeft.toLong())
            this.notifyChange()
        }

        val model = DeleteNotificationRequestModel(
            sender = item.sender
        )

        withActivityAs<MainActivity>()?.deleteNotification(model)
    }

    fun refreshSenders() {
        viewModel.getSenders(isArchived)
    }

    fun readNotification(sender: String) {
        senderAdapter?.readNotification(sender)
    }

    private fun setupViews() {
        recycler_view_sender.apply {
            senderAdapter =
                SenderAdapter(requireContext(), this@SenderFragment)
            this.adapter = senderAdapter
            this.layoutManager = LinearLayoutManager(requireContext())
        }

        text_view_archive_delete_all.setText(isArchived then R.string.label_delete_all ?: R.string.label_archive_all)
    }

    private fun setupListeners() {
        text_view_archive_delete_all.click {
            showConfirmation()
        }

        recycler_view_sender.apply {
            val layoutManager = this.layoutManager as LinearLayoutManager
            paginationListener = object : PaginationListener(layoutManager) {
                override fun loadMoreItems(nextPage: Int) {
                    viewModel.getSenders(isArchived, nextPage)
                    senderAdapter?.addLoading()
                }

                override fun hasNext(): Boolean {
                    val list = viewModel.sendersLD.value
                    return if (list != null && list.isNotEmpty()) {
                        list.last().hasNext
                    } else {
                        false
                    }
                }

                override fun isLoading(): Boolean {
                    return viewModel.isLoading.get()
                }
            }
            this.addOnScrollListener(paginationListener!!)
        }
    }

    private fun showConfirmation() {
        if (isArchived) {
            showYesNoAlertDialog(
                message = getString(R.string.prompt_delete_all),
                yesButtonText = R.string.action_yes,
                yesAction = {
                    val model = DeleteNotificationRequestModel(
                        deleteAll = true
                    )

                    withActivityAs<MainActivity>()?.deleteNotification(model)
                },
                noButtonText = R.string.action_no,
                noAction = {}
            )
        } else {
            showYesNoAlertDialog(
                message = getString(R.string.prompt_archive_all),
                yesButtonText = R.string.action_yes,
                yesAction = {
                    val model = ArchiveNotificationRequestModel(
                        archiveAll = true
                    )

                    withActivityAs<MainActivity>()?.archiveNotification(model)
                },
                noButtonText = R.string.action_no,
                noAction = {}
            )
        }
    }
}