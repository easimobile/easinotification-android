package com.easipos.easinotification.bundle

object ParcelData {

    const val CLEAR_DB = "clear_db"
    const val URL = "url"
    const val NOTIFICATION = "notification"
    const val SENDER = "sender"
    const val IS_ARCHIVED = "is_archived"
}