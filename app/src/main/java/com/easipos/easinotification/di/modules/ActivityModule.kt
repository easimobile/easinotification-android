package com.easipos.easinotification.di.modules

import com.easipos.easinotification.activities.login.navigation.LoginNavigation
import com.easipos.easinotification.activities.login.navigation.LoginNavigationImpl
import com.easipos.easinotification.activities.main.navigation.MainNavigation
import com.easipos.easinotification.activities.main.navigation.MainNavigationImpl
import com.easipos.easinotification.activities.notification.navigation.NotificationNavigation
import com.easipos.easinotification.activities.notification.navigation.NotificationNavigationImpl
import com.easipos.easinotification.activities.splash.navigation.SplashNavigation
import com.easipos.easinotification.activities.splash.navigation.SplashNavigationImpl
import io.github.anderscheow.library.di.modules.ActivityBaseModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

class ActivityModule : ActivityBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<SplashNavigation>() with provider { SplashNavigationImpl() }
            bind<LoginNavigation>() with provider { LoginNavigationImpl() }
            bind<MainNavigation>() with provider { MainNavigationImpl() }
            bind<NotificationNavigation>() with provider { NotificationNavigationImpl() }
        }
    }
}
