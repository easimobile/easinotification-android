package com.easipos.easinotification.di.modules

import android.app.Application
import com.easipos.easinotification.Easi
import com.easipos.easinotification.api.misc.AuthInterceptor
import com.easipos.easinotification.api.misc.HostSelectionInterceptor
import com.easipos.easinotification.api.misc.TokenAuthenticator
import com.easipos.easinotification.api.misc.constructOkhttpClient
import com.easipos.easinotification.api.services.Api
import com.easipos.easinotification.datasource.DataFactory
import com.easipos.easinotification.managers.FcmManager
import com.easipos.easinotification.managers.PushNotificationManager
import com.easipos.easinotification.repositories.auth.AuthDataRepository
import com.easipos.easinotification.repositories.auth.AuthRepository
import com.easipos.easinotification.repositories.notification.NotificationDataRepository
import com.easipos.easinotification.repositories.notification.NotificationRepository
import com.easipos.easinotification.repositories.precheck.PrecheckDataRepository
import com.easipos.easinotification.repositories.precheck.PrecheckRepository
import com.easipos.easinotification.services.FcmService
import com.easipos.easinotification.services.PushNotificationService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import io.github.anderscheow.library.di.modules.BaseModule
import io.github.anderscheow.library.di.modules.CommonBaseModule
import io.github.anderscheow.validator.Validator
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class CommonModule(private val easi: Easi) : CommonBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        super.provideAdditionalModule(builder)
        builder.apply {
            bind<Easi>() with singleton { instance<Application>() as Easi }
            bind<Validator>() with singleton { Validator.with(easi) }
            bind<PushNotificationManager>() with singleton { PushNotificationManager(PushNotificationService()) }
            bind<FcmManager>() with singleton { FcmManager(FcmService(easi)) }

            bind<DataFactory>() with singleton {
                DataFactory(instance(), instance(), instance(), instance())
            }

            bind<PrecheckRepository>() with singleton { PrecheckDataRepository(instance()) }
            bind<AuthRepository>() with singleton { AuthDataRepository(instance()) }
            bind<NotificationRepository>() with singleton {
                NotificationDataRepository(instance())
            }
        }
    }
}

class ApiModule(private val userAgent: String,
                private val endpoint: String,
                private val authorisation: String
) : BaseModule("apiModule") {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<AuthInterceptor>() with singleton { AuthInterceptor(userAgent, authorisation) }
            bind<HostSelectionInterceptor>() with singleton { HostSelectionInterceptor() }
            bind<TokenAuthenticator>() with singleton { TokenAuthenticator(instance()) }
            bind<RxJava2CallAdapterFactory>() with singleton { RxJava2CallAdapterFactory.create() }
            bind<GsonConverterFactory>() with singleton {
                val gson = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                    .create()
                GsonConverterFactory.create(gson)
            }
            bind<Api>() with singleton {
                Retrofit.Builder()
                    .baseUrl(endpoint)
                    .client(constructOkhttpClient(listOf(
                        instance<AuthInterceptor>(), instance<HostSelectionInterceptor>()
                    ), instance<TokenAuthenticator>()))
                    .addCallAdapterFactory(instance())
                    .addConverterFactory(instance())
                    .build()
                    .create(Api::class.java)
            }
        }
    }
}