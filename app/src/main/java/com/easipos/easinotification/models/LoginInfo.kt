package com.easipos.easinotification.models

data class LoginInfo(
    val apiKey: String
)