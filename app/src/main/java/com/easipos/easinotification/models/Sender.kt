package com.easipos.easinotification.models

import android.content.Context
import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import io.github.anderscheow.library.kotlinExt.formatDate
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Sender(
    val sender: String,
    val latestNotificationBody: String,
    val latestCreatedAt: String,
    val isRead: Boolean,
    val isArchived: Boolean
) : Parcelable {

    companion object {

        var EMPTY = Sender(
            sender = "",
            latestNotificationBody = "",
            latestCreatedAt = "",
            isRead = false,
            isArchived = false,
        )

        var DIFF_CALLBACK: DiffUtil.ItemCallback<Sender> = object : DiffUtil.ItemCallback<Sender>() {
            override fun areItemsTheSame(oldItem: Sender, newItem: Sender): Boolean {
                return oldItem.sender == newItem.sender
            }

            override fun areContentsTheSame(oldItem: Sender, newItem: Sender): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        (other as? Sender)?.let {
            return sender == other.sender &&
                    latestNotificationBody == other.latestNotificationBody &&
                    latestCreatedAt == other.latestCreatedAt &&
                    isRead == other.isRead &&
                    isArchived == other.isArchived
        }
        return false
    }

    override fun hashCode(): Int {
        return sender.hashCode()
    }

    fun formatCreatedAt(context: Context): String {
        try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault()).parse(latestCreatedAt)
            if (date != null) {
                return date.time.formatDate("MMM dd")
            }
        } catch (ex: Exception) {
        }

        return latestCreatedAt
    }
}