package com.easipos.easinotification.models

import android.content.Context
import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import io.github.anderscheow.library.kotlinExt.formatDate
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Notification(
    val notificationId: String,
    val sender: String,
    val createdAt: String,
    val title: String,
    val body: String,
    val category: String,
    val attachment: String
) : Parcelable {

    companion object {

        var EMPTY = Notification(
            notificationId = "-1",
            sender = "",
            createdAt = "",
            title = "",
            body = "",
            category = "",
            attachment = ""
        )

        var DIFF_CALLBACK: DiffUtil.ItemCallback<Notification> = object : DiffUtil.ItemCallback<Notification>() {
            override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
                return oldItem.notificationId == newItem.notificationId
            }

            override fun areContentsTheSame(oldItem: Notification, newItem: Notification): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        (other as? Notification)?.let {
            return notificationId == other.notificationId &&
                    sender == other.sender &&
                    createdAt == other.createdAt &&
                    title == other.title &&
                    body == other.body &&
                    category == other.category &&
                    attachment == other.attachment
        }
        return false
    }

    override fun hashCode(): Int {
        return notificationId.hashCode()
    }

    fun formatCreatedAt(context: Context): String {
        try {
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault()).parse(createdAt)
            if (date != null) {
                return date.time.formatDate("HH:mm dd/MM/yyyy")
            }
        } catch (ex: Exception) {
        }

        return createdAt
    }
}