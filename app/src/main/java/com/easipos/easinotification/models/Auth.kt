package com.easipos.easinotification.models

data class Auth(val token: String)