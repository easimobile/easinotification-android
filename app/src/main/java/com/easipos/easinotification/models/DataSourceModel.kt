package com.easipos.easinotification.models

class DataSourceListModel<out T>(val itemList: List<T>, val totalOfElements: Long, val hasNext: Boolean, val currentPage: Int)

class DataSourceMapModel<K, out V>(val itemMap: Map<K, V>, val totalOfElements: Long, val hasNext: Boolean)